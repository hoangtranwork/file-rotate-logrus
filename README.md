#### Minimal, opinionated [file-rotatelogs](https://github.com/lestrrat-go/file-rotatelogs) hook for [logrus](https://github.com/sirupsen/logrus)

```go
    hook, err := NewHook(
        RotateLogOpts{
            Pattern:       "./log/rotate-me.%Y%m%d.%H%M.log",
            RotationTime:  1 * time.Minute,
            RotationCount: 2,
        },
        &logrus.JSONFormatter{PrettyPrint: false},
        logrus.InfoLevel,
    )
    if err != nil {
        panic(err)
    }

    var log = logrus.New()
    log.AddHook(hook)
    log.Info("This is a info log")
    log.Warn("This is a warning log")
```

The directory will be created if not existed (when NewHook() is called).
