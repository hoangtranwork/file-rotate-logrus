package file_rotate_logrus

// package main

import (
	"github.com/lestrrat-go/file-rotatelogs"
	"github.com/sirupsen/logrus"
	"os"
	"path"
	"time"
)

type RotateLogOpts struct {
	Pattern       string
	RotationTime  time.Duration
	RotationCount uint
	LinkName      string
}

func NewHook(opts RotateLogOpts, formatter logrus.Formatter, minLevel logrus.Level) (hook *Hook, err error) {
	// create parent dirs if not existed yet
	dir := path.Dir(opts.Pattern)
	err = os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		return
	}

	var options []rotatelogs.Option
	options = append(options, rotatelogs.WithRotationTime(opts.RotationTime))
	options = append(options, rotatelogs.WithRotationCount(opts.RotationCount))
	options = append(options, rotatelogs.WithLinkName(opts.LinkName))
	options = append(options, rotatelogs.WithMaxAge(-1)) // hard coded disabled max age

	rl, err := rotatelogs.New(opts.Pattern, options...)
	if err != nil {
		return
	}

	hook = &Hook{
		rotateLogs: rl,
		formatter:  formatter,
		minLevel:   minLevel,
	}

	return
}

type Hook struct {
	rotateLogs *rotatelogs.RotateLogs
	formatter  logrus.Formatter
	minLevel   logrus.Level
}

func (hook *Hook) Fire(entry *logrus.Entry) error {
	msg, err := hook.formatter.Format(entry)
	if err != nil {
		return err
	}

	_, err = hook.rotateLogs.Write([]byte(msg))

	return err
}

func (hook *Hook) Levels() []logrus.Level {
	return logrus.AllLevels[:hook.minLevel+1]
}
