package file_rotate_logrus

import (
	"github.com/sirupsen/logrus"
	"testing"
	"time"
)

func TestRotateLog(t *testing.T) {
	hook, err := NewHook(
		RotateLogOpts{
			Pattern:       "./log/rotate-me.%Y%m%d.%H%M.log",
			RotationTime:  1 * time.Minute,
			RotationCount: 2,
			LinkName:      "./log/rotate-me.current.log",
		},
		&logrus.JSONFormatter{PrettyPrint: false},
		logrus.InfoLevel,
	)
	if err != nil {
		panic(err)
	}

	var log = logrus.New()
	log.AddHook(hook)

	for i := 0; i < 1000; i++ {
		log.Info("This is info number: ", i)
	}

	log.Warn("This is a warning log")
}

func TestRotateLogWithDirectoryNotPreExists(t *testing.T) {
	hook, err := NewHook(
		RotateLogOpts{
			Pattern:       "./not/exist/dir/rotate-me.%Y%m%d.%H%M.log",
			RotationTime:  1 * time.Minute,
			RotationCount: 2,
		},
		&logrus.JSONFormatter{PrettyPrint: false},
		logrus.InfoLevel,
	)
	if err != nil {
		panic(err)
	}

	var log = logrus.New()
	log.AddHook(hook)

	for i := 0; i < 1000; i++ {
		log.Info("This is info number: ", i)
	}

	log.Warn("This is a warning log")
}
